#include <main.h>

void setup()
{
    Serial.begin(DEBUG_BAUDRATE);
    gps_serial.begin(GPS_BAUDRATE);
    lora_serial.begin(COMM_BAUDRATE);

    LowPower.begin();
    // LowPower.enableWakeupFrom(&gps_serial, GPS_Interrupt);
    // LowPower.attachInterruptWakeup(GPS_RX, GPS_Interrupt, FALLING);
    pinMode(LED, OUTPUT);
}

void loop()
{
    LowPower.sleep();

    GPS_HandleRead();

    if (millis() > 5000 && gps.charsProcessed() < 10)
    {
        Serial.println(F("No GPS detected: check wiring."));
        while(true);
    }
}