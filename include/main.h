/* Control System Design Project Source Code
 * This code has been authored by Sencer Yazici at 2019 Fall term.
 * For furthermore details contact me by senceryazici@gmail.com
 */ 

#include <Arduino.h>
#include <TinyGPS++.h>
#include <HardwareSerial.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/BatteryState.h>
#include <STM32LowPower.h>
#include <HardwareTimer.h>
// #include <ros/msg.h>

/* ********** PARAMETERS ********** */
#define DEBUG_BAUDRATE               9600
#define GPS_BAUDRATE                 9600
#define COMM_BAUDRATE                9600
#define BUFFER_SIZE                  512

// For Bluepill F103C8T6
#define GPS_RX                       PA3
#define GPS_TX                       PA2
#define LORA_RX                      PB11
#define LORA_TX                      PB10
#define LED                          LED_BUILTIN

// For Nucleo-F429ZI Board
// #define GPS_RX                       PD2
// #define GPS_TX                       PC12
// #define LORA_RX                      PD6
// #define LORA_TX                      PD5
// #define LED                          LED_GREEN
HardwareSerial gps_serial(GPS_RX, GPS_TX);
HardwareSerial lora_serial(LORA_RX, LORA_TX);
TinyGPSPlus gps;

sensor_msgs::NavSatFix msg;
sensor_msgs::BatteryState battery;
uint32_t msg_count = 0;


uint32_t count = 0;

/**
 * @brief This publish message is a very basic implementation of what's used for ROS.
 * The idea is to use ROS built-in sensor_msgs/NavSatFix message, and use the 
 * basic serialization and deserialization advantages of the class. As streaming
 * interface, I'll use LoRa's Serial interface. This might be a base for future 
 * work of Radio communcation implementation for rosserial, which basically
 * uses the UART to communicate to radio, rather than directly to PC.
 * @param id ID of the message, which states the type of the request, in this case 0 (Publisher)
 * @returns -1 if the length of message is overflow for buffer.
 */
int publish(int id, const ros::Msg * msg)
{
    uint8_t message_out[BUFFER_SIZE];
    if (id >= 100)
      return 0;

    /* serialize message */
    int l = msg->serialize(message_out + 7);

    /* setup the header */
    message_out[0] = 0xff;
    message_out[1] = 0xfe; // PROTOCOL_VER = 0xfe
    message_out[2] = (uint8_t)((uint16_t)l & 255);
    message_out[3] = (uint8_t)((uint16_t)l >> 8);
    message_out[4] = 255 - ((message_out[2] + message_out[3]) % 256);
    message_out[5] = (uint8_t)((int16_t)id & 255);
    message_out[6] = (uint8_t)((int16_t)id >> 8);

    /* calculate checksum */
    int chk = 0;
    for (int i = 5; i < l + 7; i++)
      chk += message_out[i];
    l += 7;
    message_out[l++] = 255 - (chk % 256);

    if (l <= BUFFER_SIZE)
    {
        // hardware_.write(message_out, l);
        lora_serial.write(message_out, l);
        lora_serial.println();
        return l;
    }
    else
    {
        // logerror("Message from device dropped: message larger than buffer.");
        return -1;
    }
}


void GPS_Interrupt()
{
    // Serial.println("LLP");
}

void GPS_HandleRead()
{
    while (gps_serial.available() > 0)
    {
        gps.encode(gps_serial.read());
    }

    bool valid = gps.location.isValid() && gps.time.isValid() && gps.date.isValid() && gps.altitude.isValid();
    bool updated = gps.location.isUpdated() && gps.time.isUpdated() && gps.date.isUpdated() && gps.altitude.isUpdated();

    if (updated && valid)
    {
        // Create 1 Pulse, timer stops itself after a shot.
        digitalToggle(LED);

        msg.altitude = gps.altitude.meters();
        msg.latitude = gps.location.lat();
        msg.longitude = gps.location.lng();
        msg.header.seq = (uint32_t)msg_count++;
        
        // uint32_t _millis = millis();
        // int seconds = (int)( _millis / 1000.0);
        // int nsecs = (_millis - seconds * 1000) * 1000.0;
        // msg.header.stamp.sec = seconds;
        // msg.header.stamp.nsec = nsecs;

        uint32_t sec = 0;
        sec += (int)gps.time.second();
        sec += (int)gps.time.minute() * (int)60;
        sec += (uint32_t)gps.time.hour() * (uint32_t)3600;
        msg.header.stamp.sec = sec;
        msg.header.stamp.nsec = (uint32_t)gps.time.centisecond() * (uint32_t)10000000;
        msg.header.frame_id = "car_0";  

        uint8_t ser[BUFFER_SIZE];
        // int size = msg.serialize(ser);
        int size = publish(0, &msg);
        memset(ser, 0, BUFFER_SIZE * (sizeof(uint8_t)) );
        delay(200);
        
        battery.header = msg.header;
        battery.location = "main";
        battery.voltage = 12.8;
        battery.current = -1.23;
        battery.percentage = 23;
        size = publish(1, &battery);
    }
}